class NoPluginFoundException(Exception):
    def __init__(self, uri):
        super().__init__('No plugin found for uri ' + uri)

class PluginException(Exception):
    def __init__(self, trace):
        super().__init__('An exception has been triggered during plugin excecution : \n' + trace)

class BadLinkException(Exception):
    def __init__(self, uri):
        super().__init__(uri + ' is a bad link')
