import graphene
from Slibarr.plugins import sources, link_hosters, hosters


class Link(graphene.ObjectType):
    name = graphene.String()
    hoster = graphene.String()
    link = graphene.String()
    real_link = graphene.String()

    def resolve_real_link(self, info):
        return link_hosters.resolve_url(self.link)


class Entity(graphene.Interface):
    uri = graphene.String()
    source = graphene.String()
    quality = graphene.String()
    language = graphene.String()
    links = graphene.List(Link)

    def resolve_links(self, info):
        return sources.get_links(self)


class SearchResult(graphene.Interface):
    uri = graphene.String()
    source = graphene.String()
    name = graphene.String()
    entities = graphene.List(Entity)

    def resolve_entities(self, info):
        return sources.get_all_entities(self.uri)


class Movie(graphene.ObjectType):
    class Meta:
        interfaces = (SearchResult, )


class MovieEntity(graphene.ObjectType):
    class Meta:
        interfaces = (Entity, )


class TVShow(graphene.ObjectType):
    class Meta:
        interfaces = (SearchResult, )


class Episode(graphene.ObjectType):
    class Meta:
        interfaces = (Entity, )

    n = graphene.Int()


class Season(graphene.ObjectType):
    class Meta:
        interfaces = (Entity, )

    n = graphene.Int()
    episodes = graphene.List(Episode)

    def resolve_episodes(self, info):
        return sources.get_episodes(self)


class PluginType(graphene.Enum):
    HOSTER = 1
    LINK_HOSTER = 2
    SOURCE = 3


class Plugin(graphene.ObjectType):
    name = graphene.String()
    uri_regex = graphene.String()
    plugin_type = graphene.Field(PluginType)


class Query(graphene.ObjectType):
    search = graphene.List(SearchResult, q=graphene.String(required=True))
    result = graphene.Field(SearchResult, uri=graphene.String(required=True))
    entity = graphene.Field(Entity, uri=graphene.String(required=True))
    movie = graphene.Field(Movie, uri=graphene.String(required=True))
    tv_show = graphene.Field(TVShow, uri=graphene.String(required=True))
    movie_entity = graphene.Field(
        MovieEntity, uri=graphene.String(required=True))
    season = graphene.Field(Season, uri=graphene.String(required=True))
    real_link = graphene.String(link=graphene.String(required=True))
    unrestricted_link = graphene.String(link=graphene.String(required=True))

    enabled_plugins = graphene.List(Plugin)

    def resolve_search(self, info, q):
        return sources.search(q)

    def resolve_result(self, info, uri):
        return sources.get_result(uri)

    def resolve_entity(self, info, uri):
        return sources.get_entity(uri)

    def resolve_movie(self, info, uri):
        return sources.get_result(uri)

    def resolve_tv_show(self, info, uri):
        return sources.get_result(uri)

    def resolve_movie_entity(self, info, uri):
        return sources.get_entity(uri)

    def resolve_season(self, info, uri):
        return sources.get_entity(uri)

    def resolve_real_link(self, info, link):
        return link_hosters.resolve_url(link)

    def resolve_unrestricted_link(self, info, link):
        return hosters.resolve_url(link)

    def resolve_enabled_plugins(self, info):
        plugins = []
        for t in [(hosters, PluginType.HOSTER),
                  (link_hosters, PluginType.LINK_HOSTER), (sources,
                                                           PluginType.SOURCE)]:
            for p in t[0].plugins:
                plugins.append(
                    Plugin(
                        name=p.name, uri_regex=p.uri_regex, plugin_type=t[1]))
        return plugins


schema = graphene.Schema(query=Query)
