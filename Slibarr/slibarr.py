from flask import Flask
from flask_graphql import GraphQLView
from Slibarr.schema import schema

app = Flask(__name__)

app.add_url_rule(
    '/',
    view_func=GraphQLView.as_view('graphql', schema=schema, graphiql=False))

if __name__ == "__main__":
    app.run()
