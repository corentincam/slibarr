import re
import time
import requests

from Slibarr.config import config
from Slibarr.exceptions import BadLinkException
from .HosterInterface import HosterInterface


class Uptobox(HosterInterface):
    name = 'Uptobox'
    uri_regex = r'https?://uptobox\.com/.*'

    def resolve_url(self, url):
        url = url.split('?')[0]
        file_code = re.sub(r'(?is)https?://uptobox.com/', '', url)
        params = ''
        if 'Uptobox' in config.plugins['hosters'].keys(
        ) and config.plugins['hosters']['Uptobox'] is not None:
            if 'token' in config.plugins['hosters']['Uptobox'].keys():
                params = '&token=' + config.plugins['hosters']['Uptobox']['token']
        resp = requests.get('https://uptobox.com/api/link?file_code=' +
                            file_code + params)
        if resp.status_code == 200:
            if resp.json()['data'] != []:
                while 'waiting' in resp.json()['data'].keys():
                    if resp.json()['data']['waiting'] >= 60:
                        return None
                    time.sleep(int(resp.json()['data']['waiting']) + 1)
                    resp = requests.get(
                        'https://uptobox.com/api/link?file_code=' + file_code +
                        params + '&waitingToken=' +
                        resp.json()['data']['waitingToken'])
                if 'dlLink' in resp.json()['data'].keys():
                    return resp.json()['data']['dlLink']
        raise BadLinkException(url)
