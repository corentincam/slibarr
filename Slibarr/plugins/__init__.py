import re
import cloudscraper
import functools
import traceback
from Slibarr.config import config
from Slibarr.exceptions import NoPluginFoundException, PluginException
from Slibarr.plugins.sources.SourceInterface import SourceInterface
from Slibarr.plugins.link_hosters.LinkHosterInterface import LinkHosterInterface
from Slibarr.plugins.hosters.HosterInterface import HosterInterface


def get_enabled_plugins(import_path, config_key):
    enabled_plugins = []
    for plugin in config.plugins[config_key]:
        m = __import__(import_path + plugin, fromlist=[plugin])
        c = getattr(m, plugin)
        enabled_plugins.append(c)
    return enabled_plugins


enabled_sources = get_enabled_plugins('Slibarr.plugins.sources.',
                                      'enabled-sources')
enabled_link_hosters = get_enabled_plugins('Slibarr.plugins.link_hosters.',
                                           'enabled-link-hosters')
enabled_hosters = get_enabled_plugins('Slibarr.plugins.hosters.',
                                      'enabled-hosters')

def exception_catching(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception:
            raise PluginException(traceback.format_exc())
    return wrapper


class Plugins:
    def __init__(self, plugins, with_redirection=False):
        self.plugins = plugins
        self.with_redirection = with_redirection
        if with_redirection:
            self.session = cloudscraper.create_scraper(delay=10)

    def _get_plugin_for_uri(self, uri):
        for p in self.plugins:
            if re.match(p.uri_regex, uri):
                return p
        raise NoPluginFoundException(uri)

    def _get_plugin(self, uri):
        if self.with_redirection:
            r = self.session.get(uri, allow_redirects=False)
            if 'location' in r.headers:
                uri = r.headers['location']
            try:
                plugin = self._get_plugin_for_uri(uri)
            except NoPluginFoundException:
                plugin = None
            if plugin is None or r.status_code == 302:
                return self._get_plugin(uri)
            return plugin, uri
        return self._get_plugin_for_uri(uri)


class Sources(Plugins, SourceInterface):
    def __init__(self):
        Plugins.__init__(self, [p() for p in enabled_sources])

    @exception_catching
    def search(self, query):
        results = []
        for p in self.plugins:
            results.extend(p.search(query))
        return results

    @exception_catching
    def get_result(self, uri):
        s = self._get_plugin(uri)
        if s is not None:
            return s.get_result(uri)
        return None

    @exception_catching
    def get_all_entities(self, uri):
        s = self._get_plugin(uri)
        if s is not None:
            return s.get_all_entities(uri)
        return []

    @exception_catching
    def get_entity(self, uri):
        s = self._get_plugin(uri)
        if s is not None:
            return s.get_entity(uri)
        return None

    @exception_catching
    def get_episodes(self, season):
        s = self._get_plugin(season.uri)
        if s is not None:
            return s.get_episodes(season)
        return []

    @exception_catching
    def get_links(self, entity):
        s = self._get_plugin(entity.uri)
        if s is not None:
            return s.get_links(entity)
        return []


class LinkHoster(Plugins, LinkHosterInterface):
    def __init__(self):
        Plugins.__init__(self, [p() for p in enabled_link_hosters], True)

    @exception_catching
    def resolve_url(self, url):
        lh, url = self._get_plugin(url)
        if lh is not None:
            return lh.resolve_url(url)
        return None


class Hoster(Plugins, HosterInterface):
    def __init__(self):
        Plugins.__init__(self, [p() for p in enabled_hosters])

    @exception_catching
    def resolve_url(self, url):
        h = self._get_plugin(url)
        if h is not None:
            return h.resolve_url(url)
        return None


sources = Sources()
link_hosters = LinkHoster()
hosters = Hoster()
