class SourceInterface:
    name = 'SourceName'
    uri_regex = 'uri_regex'

    def search(self, query):
        return []

    def get_result(self, uri):
        return None

    def get_all_entities(self, uri):
        return []

    def get_entity(self, uri):
        return None

    def get_episodes(self, season):
        return []

    def get_links(self, entity):
        return []
