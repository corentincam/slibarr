import yaml
import os
import shutil


class Config:
    def create_if_absent(self):
        path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..')
        if not os.path.isfile(os.path.join(path, self._filename)):
            shutil.copy(
                os.path.join(path, 'config_example.yml'),
                os.path.join(path, self._filename))

    def reload_from_file(self):
        with open(self._filename, 'r') as f:
            self._config = yaml.load(f.read(), Loader=yaml.SafeLoader)

    def write_to_file(self):
        with open(self._filename, 'w') as f:
            f.write(yaml.dump(self._config, Dumper=yaml.SafeDumper))

    def __init__(self, filename):
        self._filename = filename
        self.create_if_absent()
        self.reload_from_file()

    def __getattr__(self, attr):
        return self._config[attr]


config = Config('config.yml')
