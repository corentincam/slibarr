# Slibarr

[![pipeline status](https://gitlab.com/valtrok/slibarr/badges/master/pipeline.svg)](https://gitlab.com/valtrok/slibarr/pipelines)
[![coverage report](https://gitlab.com/valtrok/slibarr/badges/master/coverage.svg)](http://valtrok.gitlab.io/slibarr)

API provider for direct download websites.

## Install

One day it will be deployed to PyPi but for now clone this repo and install it manually.

Recommended commands:

```
git clone https://gitlab.com/valtrok/slibarr.git
cd slibarr
python3 -m venv venv
source venv/bin/activate
pip install wheel
pip install .
```

## Run

To run the GraphQL server, just run the following command in your activated virtual environment:

```
python -m Slibarr.slibarr
```

In production, it is recommended to run Slibarr with a production-ready server, like gunicorn

```
pip install gunicorn
gunicorn Slibarr.slibarr:app -w 1 --threads 5 -b 127.0.0.1:5005
```

This command will run Slibarr with 1 worker, 5 threads and listen on port 5005