import pytest
from Slibarr.plugins.sources.ZoneTelechargement import ZoneTelechargement
from Slibarr.plugins import sources
from Slibarr.exceptions import NoPluginFoundException
from Slibarr.schema import Movie, TVShow, MovieEntity, Season, Episode, Link
from Slibarr.config import config

class TestSources:
    def test_get_plugin(self):
        assert isinstance(
            sources._get_plugin(
                config.plugins['sources']['ZoneTelechargement']['URL'] + "films-gratuit/464702-witness-HDLight%201080p-MULTI.html"
            ), ZoneTelechargement)
        with pytest.raises(NoPluginFoundException):
            sources._get_plugin("https://google.fr/")

    def test_search(self):
        results = sources.search("Witness")
        for r in results:
            assert isinstance(r, (Movie, TVShow))
        results = sources.search("q")
        assert len(results) == 0
        results = sources.search("qu")
        assert len(results) == 0
        results = sources.search("que")
        assert len(results) > 0

    def test_get_result(self):
        result = sources.get_result(
            config.plugins['sources']['ZoneTelechargement']['URL'] + "films-gratuit/464702-witness-HDLight%201080p-MULTI.html"
        )
        assert isinstance(result, Movie)
        result = sources.get_result(
            config.plugins['sources']['ZoneTelechargement']['URL'] + "telecharger-series/6140-brooklyn-nine-nine-HD%20720p-French.html"
        )
        assert isinstance(result, TVShow)

    def test_get_all_entities(self):
        results = sources.get_all_entities(
            config.plugins['sources']['ZoneTelechargement']['URL'] + "films-gratuit/464702-witness-HDLight%201080p-MULTI.html"
        )
        for r in results:
            assert isinstance(r, (MovieEntity, Season))

    def test_get_entity(self):
        result = sources.get_entity(
            config.plugins['sources']['ZoneTelechargement']['URL'] + "films-gratuit/464702-witness-HDLight%201080p-MULTI.html"
        )
        assert isinstance(result, MovieEntity)
        result = sources.get_entity(
            config.plugins['sources']['ZoneTelechargement']['URL'] + "telecharger-series/6140-brooklyn-nine-nine-HD%20720p-French.html"
        )
        assert isinstance(result, Season)

    def test_get_episodes(self):
        results = sources.get_episodes(
            sources.get_entity(
                config.plugins['sources']['ZoneTelechargement']['URL'] + "telecharger-series/6140-brooklyn-nine-nine-HD%20720p-French.html"
            ))
        for r in results:
            assert isinstance(r, Episode)

    def test_get_links(self):
        results = sources.get_links(
            sources.get_entity(
                config.plugins['sources']['ZoneTelechargement']['URL'] + "recharger-series/6140-brooklyn-nine-nine-HD%20720p-French.html"
            ))
        for r in results:
            assert isinstance(r, Link)
            assert 'Episode' not in r.name
