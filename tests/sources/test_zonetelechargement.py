from Slibarr.plugins.sources.ZoneTelechargement import ZoneTelechargement
from Slibarr.schema import Movie, TVShow


class TestZoneTelechargement:
    def test_search(self):
        zt = ZoneTelechargement()
        results = zt.search("Witness")
        for r in results:
            assert isinstance(r, (Movie, TVShow))
            assert hasattr(r, 'uri')
            assert hasattr(r, 'source')
            assert hasattr(r, 'name')
